"use strict";
let servicesItem = document.querySelector(".services-list");
servicesItem.children[0].classList.add("our-services-item-active");
servicesItem.children[0]  .querySelector(".triangle")
  .classList.add("triangle-active");
let servicesListImg = document.querySelectorAll(".services-list-img");
let list = servicesItem.children;
servicesItem.addEventListener("click", (event) => {
  if (event.target.closest("li")) {
    for (const iterator of list) {
      iterator.classList.remove("our-services-item-active");
      iterator.querySelector(".triangle").classList.remove("triangle-active");
    }
    event.target.closest("li").classList.add("our-services-item-active");
    event.target
      .closest("li")
      .querySelector(".triangle")
      .classList.add("triangle-active");
  }
  get();
});
function get(params) {
  let ident;
  for (const li of list) {
    if (li.classList.contains("our-services-item-active")) {
      ident = li.getAttribute("data-text");
      console.log(ident);
    }
    for (const li of servicesListImg) {
      li.classList.add("services-item-none");
      if (li.getAttribute("id") === ident) {
        li.classList.remove("services-item-none");
      }
    }
  }
}
get();

let listPhoto = document.querySelectorAll(".amazing-work-photo");
for (let i = 12; i < listPhoto.length; i++) {
  listPhoto[i].classList.add("hide");
}
let amazingList = document.querySelector(".amazing-work-list");
amazingList.children[0].classList.add("activ");
amazingList.addEventListener("click", (event) => {
  if (event.target.closest("li")) {
    for (const iterator of amazingList.children) {
      iterator.classList.remove("activ");
    }
    event.target.closest("li").classList.add("activ");
    let data = event.target.getAttribute("data-target");
    for (const iterator of listPhoto) {
      let select = iterator.getAttribute("data-target");
      if (select === data) {
        iterator.classList.remove("filter");
      } else if ("all" === data) {
        iterator.classList.remove("filter");
      } else {
        iterator.classList.add("filter");
      }
    }
  }
});
let loadButton = document.querySelector("#work-button");
loadButton.addEventListener("click", (e) => {
  if (e.target.closest("button")) {
    for (let i = 12; i < listPhoto.length; i++) {
      listPhoto[i].classList.toggle("hide");
    }
    loadButton.style.display = "none";
  }
});

let baner = document.querySelectorAll(".services-header-item");
let review = document.getElementsByClassName("review-content-item");
let index = 0;
let box = document.querySelector(".review-box");
baner[index].classList.add("services-header-item-activ");
review[index].classList.add("review-content-activ");
document.querySelector(".button-next").addEventListener("click", (e) => {
  review[index].classList.remove("review-content-activ");
  index++;
  if (index > review.length - 1) {
    box.append(review[0]);
    index = review.length - 1;
  }

  review[index].classList.add("review-content-activ");
  getIndex();
});
document.querySelector(".button-previous").addEventListener("click", (e) => {
  review[index].classList.remove("review-content-activ");
  index = index - 1;
  let nam = 1;
  let img = review[review.length - nam];
  if (index < 0) {
    box.prepend(img);
    index = 0;
    nam += 1;
  }
  review[index].classList.add("review-content-activ");
  getIndex();
});

document.querySelector(".review-nav").addEventListener("click", (e) => {
  if (e.target.closest("img")) {
    for (const iterator of review) {
      iterator.classList.remove("review-content-activ");
    }
    e.target.closest("img").classList.add("review-content-activ");
  }
  getIndex();
});

function getIndex() {
  for (let i = 0; i < review.length; i++) {
    if (review[i].classList.contains("review-content-activ")) {
      index = i;
      let target = review[i].getAttribute("data-target");
      for (const list of baner) {
        if (list.getAttribute("data-target") === target) {
          list.classList.add("services-header-item-activ");
        } else {
          list.classList.remove("services-header-item-activ");
        }
      }
    }
  }
}
